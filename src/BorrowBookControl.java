import java.util.ArrayList;
import java.util.List;

public class BorrowBookControl {

	private BorrowBookUI borrowBookUI;
	private Library library;
	private Member member;
	private enum CONTROL_STATE { INITIALISED, READY, RESTRICTED, SCANNING, IDENTIFIED, FINALISING, COMPLETED, CANCELLED };
	private CONTROL_STATE state;

	private List<Book> PENDING;
	private List<Loan> COMPLETED;
	private Book book;


	public BorrowBookControl() {
		this.library = library.getInstance();
		state = CONTROL_STATE.INITIALISED;
	}


	public void setUI(BorrowBookUI ui) {
		if (!state.equals(CONTROL_STATE.INITIALISED))
			throw new RuntimeException("BorrowBookControl: cannot call setUI except in INITIALISED state");

		this.borrowBookUI = ui;
		ui.setState(BorrowBookUI.UI_STATE.READY);
		state = CONTROL_STATE.READY;
	}


	public void swiped(int memberId) {
		if (!state.equals(CONTROL_STATE.READY))
			throw new RuntimeException("BorrowBookControl: cannot call cardSwiped except in READY state");

		member = library.getMember(memberId);
		if (member == null) {
			borrowBookUI.display("Invalid memberId");
			return;
		}
		if (library.canMemberBorrow(member))			//memberCanBorrow to canMemberBorrow
		{
			PENDING = new ArrayList<>();
			borrowBookUI.setState(BorrowBookUI.UI_STATE.SCANNING);
			state = CONTROL_STATE.SCANNING;
		}
		else
		{
			borrowBookUI.display("Member cannot borrow at this time");
			borrowBookUI.setState(BorrowBookUI.UI_STATE.RESTRICTED);
		}
	}


	public void Scanned(int bookId) {
		book = null;
		if (!state.equals(CONTROL_STATE.SCANNING)) {
			throw new RuntimeException("BorrowBookControl: cannot call bookScanned except in SCANNING state");
		}
		book = library.getBook(bookId);    //Book to getBook
		if (book == null) {
			borrowBookUI.display("Invalid bookId");
			return;
		}
		if (!book.available()) {
			borrowBookUI.display("Book cannot be borrowed");
			return;
		}
		PENDING.add(book);
		for (Book b : PENDING) {
			borrowBookUI.display(b.toString());
		}
		if (library.getLoansRemainingForMember(member) - PENDING.size() == 0) {		//loansRemainingForMember to
			borrowBookUI.display("Loan limit reached");
			complete();
		}
	}


	public void complete() {
		if (PENDING.size() == 0) {
			cancel();
		}
		else {
			borrowBookUI.display("\nFinal Borrowing List");
			for (Book b : PENDING) {
				borrowBookUI.display(b.toString());
			}
			COMPLETED = new ArrayList<Loan>();
			borrowBookUI.setState(BorrowBookUI.UI_STATE.FINALISING);
			state = CONTROL_STATE.FINALISING;
		}
	}


	public void commitLoans() {
		if (!state.equals(CONTROL_STATE.FINALISING)) {
			throw new RuntimeException("BorrowBookControl: cannot call commitLoans except in FINALISING state");
		}
		for (Book book : PENDING) {
			Loan loan = library.issueLoan(book, member);
			COMPLETED.add(loan);
		}
		borrowBookUI.display("Completed Loan Slip");
		for (Loan loan : COMPLETED) {
			borrowBookUI.display(loan.toString());
		}
		borrowBookUI.setState(BorrowBookUI.UI_STATE.COMPLETED);
		state = CONTROL_STATE.COMPLETED;
	}


	public void cancel() {													// method name 'cancel' changed to 'Cancel' by Raj_11665121
		borrowBookUI.setState(BorrowBookUI.UI_STATE.CANCELLED);
		state = CONTROL_STATE.CANCELLED;
	}


}
