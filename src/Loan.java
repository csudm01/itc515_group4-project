/**
 * 		Author
 * Student: Topman Garbuja 
 * Id : 11661760
 */

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;

@SuppressWarnings("serial")
public class Loan implements Serializable {
	
	public static enum LOAN_STATE { CURRENT, OVER_DUE, DISCHARGED };
	
	private int id;				//ID to id,ID represents id
	private Book book;				//B to book and class type book has been modified to Book
	private Member member;			//M to member and class type 'member' to 'Member'
	private Date date;				//D to date
	private LOAN_STATE state;

	//class type 'book' to Book
	public Loan(int loanId, Book book, Member member, Date dueDate) {
		this.id = loanId;
		this.book = book;
		this.member = member;
		this.date = dueDate;
		this.state = LOAN_STATE.CURRENT;
	}

	
	public void checkOverDue() {
		if (state == LOAN_STATE.CURRENT &&
			Calendar.getInstance().getDate().after(date)) {	//Date() to getDate()
			this.state = LOAN_STATE.OVER_DUE;			
		}
	}

	
	public boolean isOverDue() {
		return state == LOAN_STATE.OVER_DUE;
	}

	
	public Integer getId() {
		return id;
	}


	public Date getDueDate() {
		return date;
	}
	
	
	public String toString() {
		//variable 'sdf' to simpleDateFormat
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd/MM/yyyy");

		//variable 'sb' to stringBuilder
		StringBuilder stringBuilder = new StringBuilder();
		stringBuilder.append("Loan:  ").append(id).append("\n")
		  .append("  Borrower ").append(member.getId()).append(" : ")
		  .append(member.getLastName()).append(", ").append(member.getFirstName()).append("\n")
		  .append("  Book ").append(book.bookID()).append(" : " )	//ID() to id(),now to bookID()
		  .append(book.title()).append("\n")		//Title() to title()
		  .append("  DueDate: ").append(simpleDateFormat.format(date)).append("\n")
		  .append("  State: ").append(state);		
		return stringBuilder.toString();
	}

	//'Member()' method is changed to 'getMember()
	public Member getMember() {
		return member;
	}

	//'Book()' method is changed to 'getBook()'
	//class type book to Book
	public Book getBook() {
		return book;
	}

	//Loan to dischargeLoan()
	public void dischargeLoan() {
		state = LOAN_STATE.DISCHARGED;		
	}

}
