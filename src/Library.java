/**
 * 		Author
 * Student: Topman Garbuja 
 * Id : 11661760
 */


import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@SuppressWarnings("serial")
//'library' to Library
public class Library implements Serializable {
	
	private static final String LIBRARY_FILE = "library.obj";
	private static final int LOAN_LIMIT = 2;
	private static final int LOAN_PERIOD = 2;
	private static final double FINE_PER_DAY = 1.0;
	private static final double MAX_FINES_OWED = 5.0;
	private static final double DAMAGE_FEE = 2.0;
		
	private static Library self;
	private int bookId;			//BID to bookId
	private int memberId;		//MID to memberId
	private int libraryId;		//LID to libraryId
	private Date loadDate;		
	
	private Map<Integer, Book> catalog;			//class type 'book' to 'Book'
	private Map<Integer, Member> members;		//class type 'member' to 'Member'
	private Map<Integer, Loan> loans;			//class type 'loan' to 'Loan'
	private Map<Integer, Loan> currentLoans;	//class type 'loan' to 'Loan'
	private Map<Integer, Book> damagedBooks;
	

	private Library() {
		catalog = new HashMap<>();
		members = new HashMap<>();
		loans = new HashMap<>();
		currentLoans = new HashMap<>();
		damagedBooks = new HashMap<>();
		bookId = 1;
		memberId = 1;		
		libraryId = 1;		
	}

	//INSTANCE() to getInstance()
	public static synchronized Library getInstance() {		
		if (self == null) {
			Path path = Paths.get(LIBRARY_FILE);			
			if (Files.exists(path)) {	
				try (ObjectInputStream lof = new ObjectInputStream(new FileInputStream(LIBRARY_FILE));) {
			    
					self = (Library) lof.readObject();
					Calendar.getInstance().setDate(self.loadDate);
					lof.close();
				}
				catch (Exception e) {
					throw new RuntimeException(e);
				}
			}
			else self = new Library();
		}
		return self;
	}

	//SAVE() to save()
	public static synchronized void save() {
		if (self != null) {
			self.loadDate = Calendar.getInstance().getDate();	//Date() to getDate()
			//lof to objectOutputStream 
			try (ObjectOutputStream objectOutputStream  = new ObjectOutputStream(new FileOutputStream(LIBRARY_FILE));) {
				objectOutputStream.writeObject(self);
				objectOutputStream.flush();
				objectOutputStream.close();	
			}
			catch (Exception e) {
				throw new RuntimeException(e);
			}
		}
	}

	//BookID to getBookId()
	public int getBookId() {
		return bookId;
	}
	
	//MemberID to getMemberId()
	public int getMemberId() {
		return memberId;
	}
	
	//nextBID() to nextBookId()
	private int nextBookId() {
		return bookId++;
	}

	//nextBID() to nextMemberId()
	private int nextMemberId() {
		return memberId++;
	}

	//nextBID() to nextLibraryId()
	private int nextLibraryId() {
		return libraryId++;
	}

	//Members() to getMembers()
	public List<Member> getMembers() {		
		return new ArrayList<Member>(members.values()); 
	}

	//Books() to getBooks()
	public List<Book> getBooks() {		
		return new ArrayList<Book>(catalog.values()); 
	}

	//class type 'loan' to 'Loan'
	//CurrentLoans() to getCurrentLoans()
	public List<Loan> getCurrentLoans() {
		return new ArrayList<Loan>(currentLoans.values());
	}

	//Add_mem() to addMember()
	public Member addMember(String lastName, String firstName, String email, int phoneNo) {		
		Member member = new Member(lastName, firstName, email, phoneNo, nextMemberId());
		members.put(member.getId(), member);		
		return member;
	}

	//Add_book() to addBook()
	//variable a to author, t to title, c to callNo
	public Book addBook(String author, String title, String callNo) {		
		//'b' variable to book
		Book book = new Book(author, title, callNo, nextBookId());
		catalog.put(book.bookID(), book);		//book method ID() to getId()
												//getId() to id(),now to bookID()
		return book;
	}

	
	public Member getMember(int memberId) {
		if (members.containsKey(memberId)) 
			return members.get(memberId);
		return null;
	}

	//Book() to getBook()
	public Book getBook(int bookId) {
		if (catalog.containsKey(bookId)) 
			return catalog.get(bookId);		
		return null;
	}

	//loanLimit() to getLoanLimit()
	public int getLoanLimit() {
		return LOAN_LIMIT;
	}

	//MemberCanBorrow() to CanMemberBorrow()
	public boolean canMemberBorrow(Member member) {		
		if (member.getNumberOfCurrentLoans() == LOAN_LIMIT ) 
			return false;
				
		if (member.getFinesOwed() >= MAX_FINES_OWED) 
			return false;
				
		for (Loan loan : member.getLoans()) 
			if (loan.isOverDue()) 
				return false;
			
		return true;
	}

	//loansRemainingForMember to getLoansRemainingForMember()
	public int getLoansRemainingForMember(Member member) {		
		return LOAN_LIMIT - member.getNumberOfCurrentLoans();
	}

	
	public Loan issueLoan(Book book, Member member) {
		Date dueDate = Calendar.getInstance().getDueDate(LOAN_PERIOD);
		Loan loan = new Loan(nextLibraryId(), book, member, dueDate);
		member.takeOutLoan(loan);
		book.borrow();			//Borrow() to borrow()
		loans.put(loan.getId(), loan);
		currentLoans.put(book.bookID(), loan);		//ID() to id(),now to bookID()
		return loan;
	}
	
	
	public Loan getLoanByBookId(int bookId) {
		if (currentLoans.containsKey(bookId)) {
			return currentLoans.get(bookId);
		}
		return null;
	}

	
	public double calculateOverDueFine(Loan loan) {
		if (loan.isOverDue()) {
			long daysOverDue = Calendar.getInstance().getDaysDifference(loan.getDueDate());
			double fine = daysOverDue * FINE_PER_DAY;
			return fine;
		}
		return 0.0;		
	}


	public void dischargeLoan(Loan currentLoan, boolean isDamaged) {
		Member member = currentLoan.getMember(); //Member() to getMember()	
		Book book  = currentLoan.getBook();		//'book' to 'Book' class type
												//Book() to getBook()
		double overDueFine = calculateOverDueFine(currentLoan);
		member.addFine(overDueFine);	
		
		member.dischargeLoan(currentLoan);
		book.isDamaged(isDamaged);		//Return() to returnDamaged(),now to isDamaged()
		if (isDamaged) {
			member.addFine(DAMAGE_FEE);
			damagedBooks.put(book.bookID(), book);		//ID() to id(),now to bookID()
		}
		currentLoan.dischargeLoan(); 		//Loan() to dischargeLoan()
		currentLoans.remove(book.bookID());		//ID() to id(),now to bookID()
	}


	public void checkCurrentLoans() {
		for (Loan loan : currentLoans.values()) {
			loan.checkOverDue();
		}		
	}


	public void repairBook(Book currentBook) {
		if (damagedBooks.containsKey(currentBook.bookID())) {		//ID() to id(),now to bookID()
			currentBook.repair();		//Repair() to repair()
			damagedBooks.remove(currentBook.bookID());				//ID() to id(),now to bookID()
		}
		else {
			throw new RuntimeException("Library: repairBook: book is not damaged");
		}
		
	}
	
	
}
