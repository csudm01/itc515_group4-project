/**
 * 		Author
 * Student: Topman Garbuja 
 * Id : 11661760
 */

import java.text.SimpleDateFormat;
import java.util.Scanner;


public class Main {
	//These are not constant as keyword 'final' is missing 
	private static Scanner scanner;			//IN to scanner		
	private static Library library;				//LIB to library & class 'library' type to 'Library'
	private static String menu;				//MENU to menu
	private static Calendar calendar;			//CAL to calendar
	private static SimpleDateFormat simpleDateFormat;	//SDF to simpleDataFormat
	
	//Get_menu() to getMenu()
	private static String getMenu() {
		//'sb' to stringBuilder
		StringBuilder stringBuilder = new StringBuilder();
		
		stringBuilder.append("\nLibrary Main Menu\n\n")
		  .append("  M  : add member\n")
		  .append("  LM : list members\n")
		  .append("\n")
		  .append("  B  : add book\n")
		  .append("  LB : list books\n")
		  .append("  FB : fix books\n")
		  .append("\n")
		  .append("  L  : take out a loan\n")
		  .append("  R  : return a loan\n")
		  .append("  LL : list loans\n")
		  .append("\n")
		  .append("  P  : pay fine\n")
		  .append("\n")
		  .append("  T  : increment date\n")
		  .append("  Q  : quit\n")
		  .append("\n")
		  .append("Choice : ");
		  
		return stringBuilder.toString();
	}


	public static void main(String[] args) {		
		try {			
			scanner = new Scanner(System.in);
			library = library.getInstance();		//INSTANCE() to getInstance()
			calendar = Calendar.getInstance();
			simpleDateFormat = new SimpleDateFormat("dd/MM/yyyy");
	
			//'m' to member & 'member' class type to 'Member'
			//Members() to getMembers()
			for (Member member : library.getMembers()) {
				output(member);
			}
			output(" ");
			
			//'b' to book
			//Books() to getBooks()
			//class type 'book' to Book
			for (Book book : library.getBooks()) {
				output(book);
			}
						
			menu = getMenu();
			
			boolean e = false;
			
			while (!e) {
				
				output("\n" + simpleDateFormat.format(calendar.getDate()));		//Date() to getDate()
				
				//'c' to choice
				String choice = input(menu);
				
				switch (choice.toUpperCase()) {
				
				case "M": 
					addMember();
					break;
					
				case "LM": 
					listMembers();
					break;
					
				case "B": 
					addBook();
					break;
					
				case "LB": 
					listBooks();
					break;
					
				case "FB": 
					fixBooks();
					break;
					
				case "L": 
					borrowBook();
					break;
					
				case "R": 
					returnBook();
					break;
					
				case "LL": 
					listCurrentLoans();
					break;
					
				case "P": 
					payFine();
					break;
					
				case "T": 
					incrementDate();
					break;
					
				case "Q": 
					e = true;
					break;
					
				default: 
					output("\nInvalid option\n");
					break;
				}
				
				//SAVE() to save()
				library.save();
			}			
		} catch (RuntimeException e) {
			output(e);
		}		
		output("\nEnded\n");
	}	

		private static void payFine() {
		new PayFineUI(new PayFineControl()).run();		
	}


	private static void listCurrentLoans() {
		output("");
		//class type 'loan' to Loan
		//CurrentLoans() to getCurrentLoans()		
		for (Loan loan : library.getCurrentLoans()) {
			output(loan + "\n");
		}		
	}



	private static void listBooks() {
		output("");
		//Books() to getBooks()
		//Book class type book to Book
		for (Book book : library.getBooks()) {
			output(book + "\n");
		}		
	}



	private static void listMembers() {
		output("");
		//'member' class type to 'Member'
		//Members() to getMembers()
		for (Member member : library.getMembers()) {
			output(member + "\n");
		}		
	}



	private static void borrowBook() {
		new BorrowBookUI(new BorrowBookControl()).run();		
	}


	private static void returnBook() {
		new ReturnBookUI(new ReturnBookControl()).run();		
	}


	private static void fixBooks() {
		new FixBookUI(new FixBookControl()).run();		
	}


	private static void incrementDate() {
		try {
			int days = Integer.valueOf(input("Enter number of days: ")).intValue();
			calendar.incrementDate(days);
			library.checkCurrentLoans();
			output(simpleDateFormat.format(calendar.getDate())); 	//Date() to getDate()
			
		} catch (NumberFormatException e) {
			 output("\nInvalid number of days\n");
		}
	}


	private static void addBook() {
		
		String author = input("Enter author: ");
		String title  = input("Enter title: ");
		String callNo = input("Enter call number: ");
		//Add_book() to addBook()
		//class type 'book' to Book
		Book book = library.addBook(author, title, callNo);
		output("\n" + book + "\n");
		
	}

	
	private static void addMember() {
		try {
			String lastName = input("Enter last name: ");
			String firstName  = input("Enter first name: ");
			String email = input("Enter email: ");
			int phoneNo = Integer.valueOf(input("Enter phone number: ")).intValue();
			//Add_mem() to addMember()
			Member member = library.addMember(lastName, firstName, email, phoneNo);
			output("\n" + member + "\n");
			
		} catch (NumberFormatException e) {
			 output("\nInvalid phone number\n");
		}
		
	}


	private static String input(String prompt) {
		System.out.print(prompt);
		return scanner.nextLine();
	}
	
	
	
	private static void output(Object object) {
		System.out.println(object);
	}

	
}
