/**
 * Author
 * Student: Praveen Kumar Kokkerapati 
 * Id : 11661088
 */

import java.util.Scanner;


public class PayFineUI {


	public static enum UI_STATE { INITIALISED, READY, PAYING, COMPLETED, CANCELLED };

	private PayFineControl payFineControl;  //Modified Object name
	private Scanner scanner; //modified object name
	private UI_STATE state;

	
	public PayFineUI(PayFineControl payFineControl) {
		this.payFineControl = payFineControl;
		scanner = new Scanner(System.in);
		state = UI_STATE.INITIALISED;
		payFineControl.setUI(this);
	}
	
	
	public void setState(UI_STATE state) {
		this.state = state;
	}


	public void run() {
		output("Pay Fine Use Case UI\n");
		
		while (true) {
			
			switch (state) {
			
			case READY:
				String memSwipeStr = input("Swipe member card (press <enter> to cancel): ");
				if (memSwipeStr.length() == 0) {
					payFineControl.cancel();
					break;
				}
				try {
					int memberId = Integer.valueOf(memSwipeStr).intValue();
					payFineControl.cardSwiped(memberId);
				}
				catch (NumberFormatException e) {
					output("Invalid memberId");
				}
				break;
				
			case PAYING:
				double amount = 0;
				String amtStr = input("Enter amount (<Enter> cancels) : ");
				if (amtStr.length() == 0) {
					payFineControl.cancel();
					break;
				}
				try {
					amount = Double.valueOf(amtStr).doubleValue();
				}
				catch (NumberFormatException e) {}
				if (amount <= 0) {
					output("Amount must be positive");
					break;
				}
				payFineControl.payFine(amount);
				break;
								
			case CANCELLED:
				output("Pay Fine process cancelled");
				return;
			
			case COMPLETED:
				output("Pay Fine process complete");
				return;
			
			default:
				output("Unhandled state");
				throw new RuntimeException("FixBookUI : unhandled state :" + state);			
			
			}		
		}		
	}

	
	private String input(String prompt) {
		System.out.print(prompt);
		return scanner.nextLine();
	}	
		
		
	private void output(Object object) {
		System.out.println(object);
	}	
			

	public void display(Object object) {
		output(object);
	}

}
