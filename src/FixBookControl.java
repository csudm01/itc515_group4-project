/**
 * 		Author
 * Student: Roshan Paudel
 * Id : 11663090
 */
 public class FixBookControl {

	private FixBookUI fixBookUI;
	private enum CONTROL_STATE { INITIALISED, READY, FIXING };
	private CONTROL_STATE controlState;

	private Library library;	//Class Name changed from library to Library
	private Book currentBook;	//Class Name changed from book to book


	public FixBookControl() {
		this.library = library.getInstance();  //method name changed from Instance to getInstance
		controlState = CONTROL_STATE.INITIALISED;
	}


	public void setUI(FixBookUI ui) {
		if (!controlState.equals(CONTROL_STATE.INITIALISED)) {
			throw new RuntimeException("FixBookControl: cannot call setUI except in INITIALISED controlState");
		}
		this.fixBookUI = ui;
		ui.setState(FixBookUI.UI_STATE.READY);
		controlState = CONTROL_STATE.READY;
	}


	public void bookScanned(int bookId) {
		if (!controlState.equals(CONTROL_STATE.READY)) {
			throw new RuntimeException("FixBookControl: cannot call bookScanned except in READY controlState");
		}
		currentBook = library.getBook(bookId);

		if (currentBook == null) {
			fixBookUI.display("Invalid bookId");
			return;
		}
		if (!currentBook.damaged()) {  //variable renamed from Damaged to damaged
			fixBookUI.display("\"Book has not been damaged");
			return;
		}
		fixBookUI.display(currentBook.toString());
		fixBookUI.setState(FixBookUI.UI_STATE.FIXING);
		controlState = CONTROL_STATE.FIXING;
	}


	public void fixBook(boolean fix) {
		if (!controlState.equals(CONTROL_STATE.FIXING)) {
			throw new RuntimeException("FixBookControl: cannot call fixBook except in FIXING controlState");
		}
		if (fix) {
			library.repairBook(currentBook);
		}
		currentBook = null;
		fixBookUI.setState(FixBookUI.UI_STATE.READY);
		controlState = CONTROL_STATE.READY;
	}


	public void scanningComplete() {
		if (!controlState.equals(CONTROL_STATE.READY)) {
			throw new RuntimeException("FixBookControl: cannot call scanningComplete except in READY controlState");
		}
		fixBookUI.setState(FixBookUI.UI_STATE.COMPLETED);
	}






}
