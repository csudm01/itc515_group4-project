/**
 * Author
 * Student: Praveen Kumar Kokkerapati 
 * Id : 11661088
 */

import java.util.Scanner;


public class ReturnBookUI {

	public static enum UI_STATE { INITIALISED, READY, INSPECTING, COMPLETED };

	private ReturnBookControl returnBookControl; //Modified the object name
	private Scanner scanner; //Modified the object name
	private UI_STATE state; 

	
	public ReturnBookUI(ReturnBookControl returnBookControl) {
		this.returnBookControl = returnBookControl;
		scanner = new Scanner(System.in);
		state = UI_STATE.INITIALISED;
		returnBookControl.setUI(this);
	}


	public void run() {		
		output("Return Book Use Case UI\n");
		
		while (true) {
			
			switch (state) {
			
			case INITIALISED:
				break;
				
			case READY:
				String bookStr = input("Scan Book (<enter> completes): ");
				if (bookStr.length() == 0) {
					returnBookControl.scanningComplete();
				}
				else {
					try {
						int bookId = Integer.valueOf(bookStr).intValue();
						returnBookControl.bookScanned(bookId);
					}
					catch (NumberFormatException e) {
						output("Invalid bookId");
					}					
				}
				break;				
				
			case INSPECTING:
				String inspectAns = input("Is book damaged? (Y/N): "); //ans has been replaced by inspectAns
				boolean isDamaged = false;
				if (inspectAns.toUpperCase().equals("Y")) {					
					isDamaged = true;
				}
				returnBookControl.dischargeLoan(isDamaged);
			
			case COMPLETED:
				output("Return processing complete");
				return;
			
			default:
				output("Unhandled state");
				throw new RuntimeException("ReturnBookUI : unhandled state :" + state);			
			}
		}
	}

	
	private String input(String prompt) {
		System.out.print(prompt);
		return scanner.nextLine();
	}	
		
		
	private void output(Object object) {
		System.out.println(object);
	}
	
			
	public void display(Object object) {
		output(object);
	}
	
	public void setState(UI_STATE state) {
		this.state = state;
	}

	
}
