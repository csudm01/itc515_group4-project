/**
 * Author
 * Student: Praveen Kumar Kokkerapati 
 * Id : 11661088
 */

public class PayFineControl {
	
	private PayFineUI payFineUI;
	private enum CONTROL_STATE { INITIALISED, READY, PAYING, COMPLETED, CANCELLED };
	private CONTROL_STATE state;
	
	private Library library; //library has been modified to Library 
	private Member member;   //member has been modified to Member and errors removed


	public PayFineControl() {
		this.library = Library.getInstance(); //Function name modified
		state = CONTROL_STATE.INITIALISED; 
	}
	
	
	public void setUI(PayFineUI payFineUI) {
		if (!state.equals(CONTROL_STATE.INITIALISED)) {
			throw new RuntimeException("PayFineControl: cannot call setUI except in INITIALISED state");
		}	
		this.payFineUI = payFineUI;
		payFineUI.setState(PayFineUI.UI_STATE.READY);
		state = CONTROL_STATE.READY;		
	}


	public void cardSwiped(int memberId) {
		if (!state.equals(CONTROL_STATE.READY)) {
			throw new RuntimeException("PayFineControl: cannot call cardSwiped except in READY state");
		}	
		member = library.getMember(memberId);
		
		if (member == null) {
			payFineUI.display("Invalid Member Id");
			return;
		}
		payFineUI.display(member.toString());
		payFineUI.setState(PayFineUI.UI_STATE.PAYING);
		state = CONTROL_STATE.PAYING;
	}
	
	
	public void cancel() {
		payFineUI.setState(PayFineUI.UI_STATE.CANCELLED);
		state = CONTROL_STATE.CANCELLED;
	}


	public double payFine(double amount) {
		if (!state.equals(CONTROL_STATE.PAYING)) {
			throw new RuntimeException("PayFineControl: cannot call payFine except in PAYING state");
		}	
		double change = member.payFine(amount);
		if (change > 0) {
			payFineUI.display(String.format("Change: $%.2f", change));
		}
		payFineUI.display(member.toString());
		payFineUI.setState(PayFineUI.UI_STATE.COMPLETED);
		state = CONTROL_STATE.COMPLETED;
		return change;
	}
	


}
