import java.io.Serializable;


@SuppressWarnings("serial")
public class Book implements Serializable {            //class "book" changed to "Book" Raj_1665121;

	private String title;									// variable 'T' changed to 'title'
	private String author;									// variable 'A' changed to 'authoe'
	private String callNo;									// variable 'C' changed to 'callNo'
	private int bookID;										// variable 'ID' changed to 'id'

	private enum STATE { AVAILABLE, ON_LOAN, DAMAGED, RESERVED };
	private STATE state;


	public Book(String author, String title, String callNo, int id) {
		this.author = author;								// variable 'A' changed to 'author'
		this.title = title; 									// variable 'T' changed to 'title'
		this.callNo = callNo;									// variable 'C' changed to 'callNo'
		this.bookID = id;									// variable 'ID' changed to 'id'
		this.state = STATE.AVAILABLE;
	}


	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("Book: ").append(bookID).append("\n")
		  .append("  Title:  ").append(title).append("\n")
		  .append("  Author: ").append(author).append("\n")
		  .append("  CallNo: ").append(callNo).append("\n")
		  .append("  State:  ").append(state);

		return sb.toString();
	}


	public Integer bookID() {
		return bookID;
	}


	public String title() {
		return title;
	}



	public boolean available() {
		return ( state == STATE.AVAILABLE );
	}


	public boolean on_loan() {
		return ( state == STATE.ON_LOAN );
	}


	public boolean damaged() {
		return ( state == STATE.DAMAGED);
	}


	public void borrow() {
		if (state.equals(STATE.AVAILABLE)) {
			state = STATE.ON_LOAN;
		}
		else {
			throw new RuntimeException(String.format("Book: cannot borrow while book is in state: %s", state));
		}

	}


	public void isDamaged(boolean DAMAGED) {
		if (state.equals(STATE.ON_LOAN)) {
			if (DAMAGED) {
				state = STATE.DAMAGED;
			}
			else {
				state = STATE.AVAILABLE;
			}
		}
		else {
			throw new RuntimeException(String.format("Book: cannot Return while book is in state: %s", state));
		}
	}


	public void repair() {
		if (state.equals(STATE.DAMAGED)) {
			state = STATE.AVAILABLE;
		}
		else {
			throw new RuntimeException(String.format("Book: cannot repair while book is in state: %s", state));
		}
	}


}
